from src.math_operations import division


class TestMathOperations:
    def test_division_operation(self):
        # Given
        numerator = 2
        denominator = 1
        # When
        res = division(numerator, denominator)
        # Then
        assert res == 2
